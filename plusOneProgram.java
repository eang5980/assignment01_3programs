package Assignment02;
import java.util.Arrays;

public class plusOneProgram {
    
    public static int[] plusOne(int[] digits){

        for(int i=digits.length-1; i>=0;i--){
            if(digits[i] <9){
                digits[i]++;
                return digits;
            }
            digits[i] =0;
        }

        int[] newNum = new int[digits.length+1];
        newNum[0] = 1;
        return newNum;
    }

    
    public static void main(String[] args) {

        int[] intArray = new int[] { 1, 2, 3, 4 };

        Arrays.stream(plusOne(intArray)).forEach(e -> System.out.print(e + " ")); 

    }
}
