package Assignment02;
import java.util.Arrays;

public class moveZerosProgram {

    public static int[] moveZeros(int[] nums) {

        int curPos = 0;
        for (int i = 0; i < nums.length; i++) { // {0,0,1,2,3}
            if (nums[i] != 0) { // i = 2 , curPos=0 ;  i=3,curPos=1
                if (curPos != i) {
                    nums[curPos] = nums[i]; // nums[0] = nums[2] {1,0,1,2,3};  nums[1] = nums[3] {1,2,0,2,3} 
                    nums[i] = 0; // {1,0,0,2,3}; {1,2,0,0,3}; 
                }
                curPos++;
            }
        }

        return nums;
    }

    public static void main(String[] args) {

        int[] intArray = new int[] { 0, 0, 1, 2, 3 };

        Arrays.stream(moveZeros(intArray)).forEach(e -> System.out.print(e + " "));

    }
}
